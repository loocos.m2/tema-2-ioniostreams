package com.sda.secondExercise;

public class SysAdmin extends Employee{
    private Level suportLevel;

    public SysAdmin(String nume, int salariu, Level suportLevel) {
        super(nume, salariu);
        this.suportLevel = suportLevel;
    }

    public void setSuportLevel(Level suportLevel) {
        this.suportLevel = suportLevel;
    }

    @Override
    void work() {
        System.out.println(this.suportLevel);
    }

}
