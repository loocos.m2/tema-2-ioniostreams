package com.sda.secondExercise;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LoadEmployees {

    private LoadEmployees() {
    }

    public static List<Employee> load(String filePath) {
        List<Employee> list = new ArrayList<>();
        try (FileReader reader = new FileReader(filePath);
             BufferedReader in = new BufferedReader(reader)){
            String line;
            while ((line=in.readLine())!=null) {
                String[] valoriFisier = line.split(",");
                if (valoriFisier[0].equals("programator")) {
                    Employee e = new Programmer(valoriFisier[1],Integer.parseInt(valoriFisier[2]),valoriFisier[3]);
                   list.add(e);
                } else if (valoriFisier[0].equals("manager")){
                    Employee e = new SysAdmin(valoriFisier[1],Integer.parseInt(valoriFisier[2]),Level.valueOf(valoriFisier[3]));
                    list.add(e);
                }
                }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
