package com.sda.secondExercise;

public class Programmer extends Employee{
    private String language;

    public Programmer(String nume, int salariu, String language) {
        super(nume, salariu);
        this.language = language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    void work() {
        System.out.println(this.language);
    }
}
