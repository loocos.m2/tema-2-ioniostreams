package com.sda.secondExercise;

abstract class Employee {
    private String nume;
    private int salariu;

    public Employee(String nume, int salariu) {
        this.nume = nume;
        this.salariu = salariu;
    }

    abstract void work();

    public void setNume(String nume) {
        this.nume = nume;
    }

    public void setSalariu(int salariu) {
        this.salariu = salariu;
    }
}
