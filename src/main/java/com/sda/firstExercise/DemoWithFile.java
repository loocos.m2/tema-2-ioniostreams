package com.sda.firstExercise;

import java.io.File;
import java.io.IOException;

public class DemoWithFile {
    public static void main(String[] args) {

        File file = new File ("newFile.txt");
        try {
            if(file.createNewFile()){
                System.out.println("File was created! \n");
            } else {
                System.out.println("File allready exist! \n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(file.canWrite() && file.canRead()){
            System.out.println("File canWrite & canRead \n");
        } else {
            System.out.println("File cant Write & Read \n");
        }

        boolean readOnly = file.setReadOnly();
        System.out.println("File can now read only? " + readOnly + "\n");

        boolean setWritable = file.setWritable(readOnly);
        System.out.println("File can now write and read? " + setWritable + "\n");




    }
}
