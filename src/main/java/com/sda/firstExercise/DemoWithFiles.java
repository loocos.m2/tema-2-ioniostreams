package com.sda.firstExercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DemoWithFiles {
    public static void main(String[] args)  throws IOException {
        Path newPathFile = Paths.get("newPathFile.txt");

        if(newPathFile.isAbsolute()){
            System.out.println("New file created! \n");
        } else {
            System.out.println("Files allready exist! \n");
        }
    }

}
